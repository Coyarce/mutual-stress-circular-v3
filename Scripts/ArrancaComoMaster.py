#!/usr/bin/env python3

import socket
import os
from datetime import datetime
from pathlib import Path
############################## SCRIPT QUE EJECUTA TEST JMETER COMO MASTER #######################################

work_directory = Path(os.getcwd()).parent.absolute()
#Variables de Entorno
SCRIPT_NAME = 'Mutual_Circular_3446_v2.jmx'
JMETER_BIN = f"{work_directory}/jmeter/bin/jmeter.sh"
SCRIPT_FOLDER = os.getcwd()+'/'
RST_FOLDER = f'{os.getcwd()}/Result/'
RMI_LOCALPORT = 1099
LOCAL_HOST = '172.28.10.8'
SLAVE_HOST = ['172.28.10.9']
hosts = list()

def ExisteArchivo(pFile):
    try:
        f = None
        f = open(pFile)
        return True
    except IOError:
        return False
    finally:
        if (f!=None):
            f.close()

def PingOk(ip,port):
   s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
   s.settimeout(10)
   try:
      s.connect((ip, int(port)))
      s.shutdown(2)
      return True
   except:
      return False


data_name = None
trx_x_min = None

#Nombre Script
print(f'#### EJECUCION SCRIPT {SCRIPT_NAME}- MODO MASTER #### \n')

if (ExisteArchivo(f'{SCRIPT_FOLDER}{SCRIPT_NAME}')==False):
    print(f'Archivo : {SCRIPT_FOLDER}{SCRIPT_NAME} -> No Existe')
    quit()


SoloMaster = True
if len(SLAVE_HOST)>0:
    SoloMaster = False
else:
    print(f"Solo ejecucion en Maquina MASTER")

if not SoloMaster:
    HayAlmenosUno = False
    for line in SLAVE_HOST:
        host_name = line.strip()
        print(f"Verificando {host_name}")
        if (PingOk(host_name,RMI_LOCALPORT)):
            hosts.append(host_name)
            print(f'Maquina [{host_name}:{RMI_LOCALPORT}], OK.')
            HayAlmenosUno = True
        else:
           print(f'Maquina [{host_name}:{RMI_LOCALPORT}] NOK, Revise y vuelva a intentarlo, o realice la prueba sin este esclavo.')

    if (not HayAlmenosUno):
        print('## NO HAY MAQUINAS ESCLAVO, Debe ingresar al menos UNA, para comenzar la prueba')
        quit()

cant_hilos = 0
t_seg = 0

while cant_hilos<=0:
    hilos = input("¿Cuantos Hilos desea Ejecutar por Maquina ? ")
    cant_hilos = int(hilos)

while t_seg<=0:
    dura = input("¿Cuantos Tiempo es la duracion ? ")
    t_seg = int(dura)

now = datetime.now() # current date and time

UNICO_NAME = SCRIPT_NAME.replace('.jmx','')+'_'+now.strftime("%Y%m%d_%H%M%S")

script_name = SCRIPT_FOLDER + SCRIPT_NAME
rst_name = RST_FOLDER+'RST_'+UNICO_NAME+'.jtl'
rpt_folder = RST_FOLDER+'REPO_'+UNICO_NAME

print('#### DATOS PRUEBA ####')
print(f'Script: {script_name}')
print(f'Data: {data_name}')
print(f'Hilos x Maq. : {cant_hilos}')
print('Equipos a Utilizar:')
index = 0
cmd_host = ''
for h in hosts: 
    index = index + 1
    print(f'({index}) -> {h}:{RMI_LOCALPORT}') 
    if (cmd_host==''): 
        cmd_host=f'{h}:{RMI_LOCALPORT}' 
    else:
        cmd_host=cmd_host+f',{h}:{RMI_LOCALPORT}' 

# Separo la cantidad de transacciones en los esclavos, o si no tira parejo en todos.
#aux_trx = trx_x_min
#CantHost = len(hosts)
#trx_x_min = float(trx_x_min)/CantHost

#print(f'TRX x MIN:  {trx_x_min} x {CantHost} Maq. = {aux_trx} Trx. Totales')

print('-------------------------------------------')
print(f'- Resultado:')
#print(f'-- Logs   : {log_name}')
print(f'-- Data   : {rst_name}')
print(f'-- Report : {rpt_folder}')

print('\n#### -> ¿ DESEA COMENZAR LA EJECUCIÓN ? (S/N):')
YesNo = input()
if (YesNo=='S'): 
    print('-> INICIANDO')
    if os.path.isfile('jmeter.log'):
        os.remove("jmeter.log")
    
    #os.remove("*.pdf")
    
    #-JDataFile={data_name}   / lo saco va adentro del JMX
    if not SoloMaster:
        exec_command = f'{JMETER_BIN} -X -n -t {script_name} -R {cmd_host},{LOCAL_HOST}:{RMI_LOCALPORT} -l {rst_name} -e -o {rpt_folder} -JHilos={cant_hilos} -JDuracion={t_seg}'
    else:
        exec_command = f'{JMETER_BIN} -X -n -t {script_name} -l {rst_name} -e -o {rpt_folder} -JHilos={cant_hilos} -JDuracion={t_seg}'

    print(exec_command) 
    os.system(exec_command)
    
else:
    print('-> CANCELADO')


#rm jmeter-server.log
#echo Indique la IP externa del equipo: 
#read IP
#/home/opc-practica/jmeter/bin/jmeter-server -Djava.rmi.server.hostname=$IP -Dserver.rmi.localport=1099
#jmeter -n -t <test JMX file> -l <test log file> -e -o <Path to output folder>
