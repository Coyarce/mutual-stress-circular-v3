#!/bin/bash

IP=172.28.10.9
Reinicia=S

/home/usrpractia/mutual-stress-circular-v2/jmeter/bin/shutdown.sh

PID=`ps -eaf | grep jmeter | grep -v grep | awk '{print $2}'`
if [[ "" !=  "$PID" ]]; then
  echo "killing $PID"
  kill -9 $PID
fi

if [ $Reinicia != "S" ] 
then
  exit 0
fi

rm -rf *.log
rm -rf *.hprof

/home/usrpractia/mutual-stress-circular-v2/jmeter/bin/jmeter-server -Djava.rmi.server.hostname=$IP -Dserver.rmi.localport=1099
