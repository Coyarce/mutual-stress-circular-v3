/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 57.142857142857146, "KoPercent": 42.857142857142854};
    var dataset = [
        {
            "label" : "KO",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "OK",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.14285714285714285, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [0.0, 500, 1500, "010 - Vuelve al men\u00FA principal"], "isController": false}, {"data": [0.0, 500, 1500, "003 - PopUp - Click Autoriza"], "isController": false}, {"data": [0.0, 500, 1500, "001 - Descarga Documento"], "isController": false}, {"data": [1.0, 500, 1500, "002 - Busco y Click en Autorizar"], "isController": false}, {"data": [0.0, 500, 1500, "004 - Ingreso Numero Serie"], "isController": false}, {"data": [0.0, 500, 1500, "000 - Ingreso a Menu"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 7, 3, 42.857142857142854, 6727.714285714285, 0, 19568, 1170.0, 19568.0, 19568.0, 19568.0, 4.260497069204337E-9, 4.5360444794041756E-7, 0.0], "isController": false}, "titles": ["Label", "#Samples", "KO", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions\/s", "Received", "Sent"], "items": [{"data": ["010 - Vuelve al men\u00FA principal", 1, 1, 100.0, 1170.0, 1170, 1170, 1170.0, 1170.0, 1170.0, 1170.0, 0.8547008547008547, 2.111712072649573, 0.0], "isController": false}, {"data": ["003 - PopUp - Click Autoriza", 1, 0, 0.0, 9407.0, 9407, 9407, 9407.0, 9407.0, 9407.0, 9407.0, 0.10630381630700542, 15.857539598171574, 0.0], "isController": false}, {"data": ["001 - Descarga Documento", 1, 1, 100.0, 0.0, 0, 0, 0.0, 0.0, 0.0, 0.0, Infinity, Infinity, NaN], "isController": false}, {"data": ["002 - Busco y Click en Autorizar", 1, 0, 0.0, 153.0, 153, 153, 153.0, 153.0, 153.0, 153.0, 6.5359477124183005, 979.1347528594771, 0.0], "isController": false}, {"data": ["004 - Ingreso Numero Serie", 1, 1, 100.0, 179.0, 179, 179, 179.0, 179.0, 179.0, 179.0, 5.58659217877095, 14.953954259776538, 0.0], "isController": false}, {"data": ["000 - Ingreso a Menu", 2, 0, 0.0, 18092.5, 16617, 19568, 18092.5, 19568.0, 19568.0, 19568.0, 0.10220768601798856, 15.104479411539247, 0.0], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": [{"data": ["500\/SIN Documentos que descargar", 1, 33.333333333333336, 14.285714285714286], "isController": false}, {"data": ["500\/Sourced file: inline evaluation of: ``import java.util.List; import java.util.Random;  import org.openqa.selenium.WebE . . . \'\' : Typed variable declaration : Method Invocation WDS.browser.findElement : at Line: 19 : in file: inline evaluation of: ``import java.util.List; import java.util.Random;  import org.openqa.selenium.WebE . . . \'\' : WDS .browser .findElement ( By .xpath ( &quot;\\\/\\\/button[@class=\'ui button primary small continuaNumeroSerie primary-edt\']&quot; ) ) \\n\\nTarget exception: org.openqa.selenium.NoSuchElementException: no such element: Unable to locate element: {&quot;method&quot;:&quot;xpath&quot;,&quot;selector&quot;:&quot;\\\/\\\/button[@class=\'ui button primary small continuaNumeroSerie primary-edt\']&quot;}\\n  (Session info: headless chrome=97.0.4692.99)\\nFor documentation on this error, please visit: http:\\\/\\\/seleniumhq.org\\\/exceptions\\\/no_such_element.html\\nBuild info: version: \'3.14.0\', revision: \'aacccce0\', time: \'2018-08-02T20:19:58.91Z\'\\nSystem info: host: \'PRACTIACLNB0389\', ip: \'172.22.248.157\', os.name: \'Windows 10\', os.arch: \'x86\', os.version: \'10.0\', java.version: \'1.8.0_311\'\\nDriver info: org.openqa.selenium.chrome.ChromeDriver\\nCapabilities {acceptInsecureCerts: true, browserName: chrome, browserVersion: 97.0.4692.99, chrome: {chromedriverVersion: 97.0.4692.71 (adefa7837d02a..., userDataDir: C:\\\\Users\\\\CARLOS~1\\\\AppData\\\\L...}, goog:chromeOptions: {debuggerAddress: localhost:60920}, javascriptEnabled: true, networkConnectionEnabled: false, pageLoadStrategy: normal, platform: WINDOWS, platformName: WINDOWS, proxy: Proxy(direct), setWindowRect: true, strictFileInteractability: false, timeouts: {implicit: 0, pageLoad: 300000, script: 30000}, unhandledPromptBehavior: dismiss and notify, webauthn:extension:credBlob: true, webauthn:extension:largeBlob: true, webauthn:virtualAuthenticators: true}\\nSession ID: 9ca33f9fd56f1392d1b79247d966b032\\n*** Element info: {Using=xpath, value=\\\/\\\/button[@class=\'ui button primary small continuaNumeroSerie primary-edt\']}\\n in inline evaluation of: ``import java.util.List; import java.util.Random;  import org.openqa.selenium.WebE . . . \'\' at line number 19", 1, 33.333333333333336, 14.285714285714286], "isController": false}, {"data": ["500\/Sourced file: inline evaluation of: ``import java.util.List; import java.util.Random; import java.text.SimpleDateForma . . . \'\' : Method Invocation WDS.browser.findElement : at Line: 24 : in file: inline evaluation of: ``import java.util.List; import java.util.Random; import java.text.SimpleDateForma . . . \'\' : WDS .browser .findElement ( By .id ( &quot;sb-nav-close&quot; ) ) \\n\\nTarget exception: org.openqa.selenium.NoSuchElementException: no such element: Unable to locate element: {&quot;method&quot;:&quot;css selector&quot;,&quot;selector&quot;:&quot;#sb\\\\-nav\\\\-close&quot;}\\n  (Session info: headless chrome=97.0.4692.99)\\nFor documentation on this error, please visit: http:\\\/\\\/seleniumhq.org\\\/exceptions\\\/no_such_element.html\\nBuild info: version: \'3.14.0\', revision: \'aacccce0\', time: \'2018-08-02T20:19:58.91Z\'\\nSystem info: host: \'PRACTIACLNB0389\', ip: \'172.22.248.157\', os.name: \'Windows 10\', os.arch: \'x86\', os.version: \'10.0\', java.version: \'1.8.0_311\'\\nDriver info: org.openqa.selenium.chrome.ChromeDriver\\nCapabilities {acceptInsecureCerts: true, browserName: chrome, browserVersion: 97.0.4692.99, chrome: {chromedriverVersion: 97.0.4692.71 (adefa7837d02a..., userDataDir: C:\\\\Users\\\\CARLOS~1\\\\AppData\\\\L...}, goog:chromeOptions: {debuggerAddress: localhost:60920}, javascriptEnabled: true, networkConnectionEnabled: false, pageLoadStrategy: normal, platform: WINDOWS, platformName: WINDOWS, proxy: Proxy(direct), setWindowRect: true, strictFileInteractability: false, timeouts: {implicit: 0, pageLoad: 300000, script: 30000}, unhandledPromptBehavior: dismiss and notify, webauthn:extension:credBlob: true, webauthn:extension:largeBlob: true, webauthn:virtualAuthenticators: true}\\nSession ID: 9ca33f9fd56f1392d1b79247d966b032\\n*** Element info: {Using=id, value=sb-nav-close}\\n in inline evaluation of: ``import java.util.List; import java.util.Random; import java.text.SimpleDateForma . . . \'\' at line number 24", 1, 33.333333333333336, 14.285714285714286], "isController": false}]}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 7, 3, "500\/SIN Documentos que descargar", 1, "500\/Sourced file: inline evaluation of: ``import java.util.List; import java.util.Random;  import org.openqa.selenium.WebE . . . \'\' : Typed variable declaration : Method Invocation WDS.browser.findElement : at Line: 19 : in file: inline evaluation of: ``import java.util.List; import java.util.Random;  import org.openqa.selenium.WebE . . . \'\' : WDS .browser .findElement ( By .xpath ( &quot;\\\/\\\/button[@class=\'ui button primary small continuaNumeroSerie primary-edt\']&quot; ) ) \\n\\nTarget exception: org.openqa.selenium.NoSuchElementException: no such element: Unable to locate element: {&quot;method&quot;:&quot;xpath&quot;,&quot;selector&quot;:&quot;\\\/\\\/button[@class=\'ui button primary small continuaNumeroSerie primary-edt\']&quot;}\\n  (Session info: headless chrome=97.0.4692.99)\\nFor documentation on this error, please visit: http:\\\/\\\/seleniumhq.org\\\/exceptions\\\/no_such_element.html\\nBuild info: version: \'3.14.0\', revision: \'aacccce0\', time: \'2018-08-02T20:19:58.91Z\'\\nSystem info: host: \'PRACTIACLNB0389\', ip: \'172.22.248.157\', os.name: \'Windows 10\', os.arch: \'x86\', os.version: \'10.0\', java.version: \'1.8.0_311\'\\nDriver info: org.openqa.selenium.chrome.ChromeDriver\\nCapabilities {acceptInsecureCerts: true, browserName: chrome, browserVersion: 97.0.4692.99, chrome: {chromedriverVersion: 97.0.4692.71 (adefa7837d02a..., userDataDir: C:\\\\Users\\\\CARLOS~1\\\\AppData\\\\L...}, goog:chromeOptions: {debuggerAddress: localhost:60920}, javascriptEnabled: true, networkConnectionEnabled: false, pageLoadStrategy: normal, platform: WINDOWS, platformName: WINDOWS, proxy: Proxy(direct), setWindowRect: true, strictFileInteractability: false, timeouts: {implicit: 0, pageLoad: 300000, script: 30000}, unhandledPromptBehavior: dismiss and notify, webauthn:extension:credBlob: true, webauthn:extension:largeBlob: true, webauthn:virtualAuthenticators: true}\\nSession ID: 9ca33f9fd56f1392d1b79247d966b032\\n*** Element info: {Using=xpath, value=\\\/\\\/button[@class=\'ui button primary small continuaNumeroSerie primary-edt\']}\\n in inline evaluation of: ``import java.util.List; import java.util.Random;  import org.openqa.selenium.WebE . . . \'\' at line number 19", 1, "500\/Sourced file: inline evaluation of: ``import java.util.List; import java.util.Random; import java.text.SimpleDateForma . . . \'\' : Method Invocation WDS.browser.findElement : at Line: 24 : in file: inline evaluation of: ``import java.util.List; import java.util.Random; import java.text.SimpleDateForma . . . \'\' : WDS .browser .findElement ( By .id ( &quot;sb-nav-close&quot; ) ) \\n\\nTarget exception: org.openqa.selenium.NoSuchElementException: no such element: Unable to locate element: {&quot;method&quot;:&quot;css selector&quot;,&quot;selector&quot;:&quot;#sb\\\\-nav\\\\-close&quot;}\\n  (Session info: headless chrome=97.0.4692.99)\\nFor documentation on this error, please visit: http:\\\/\\\/seleniumhq.org\\\/exceptions\\\/no_such_element.html\\nBuild info: version: \'3.14.0\', revision: \'aacccce0\', time: \'2018-08-02T20:19:58.91Z\'\\nSystem info: host: \'PRACTIACLNB0389\', ip: \'172.22.248.157\', os.name: \'Windows 10\', os.arch: \'x86\', os.version: \'10.0\', java.version: \'1.8.0_311\'\\nDriver info: org.openqa.selenium.chrome.ChromeDriver\\nCapabilities {acceptInsecureCerts: true, browserName: chrome, browserVersion: 97.0.4692.99, chrome: {chromedriverVersion: 97.0.4692.71 (adefa7837d02a..., userDataDir: C:\\\\Users\\\\CARLOS~1\\\\AppData\\\\L...}, goog:chromeOptions: {debuggerAddress: localhost:60920}, javascriptEnabled: true, networkConnectionEnabled: false, pageLoadStrategy: normal, platform: WINDOWS, platformName: WINDOWS, proxy: Proxy(direct), setWindowRect: true, strictFileInteractability: false, timeouts: {implicit: 0, pageLoad: 300000, script: 30000}, unhandledPromptBehavior: dismiss and notify, webauthn:extension:credBlob: true, webauthn:extension:largeBlob: true, webauthn:virtualAuthenticators: true}\\nSession ID: 9ca33f9fd56f1392d1b79247d966b032\\n*** Element info: {Using=id, value=sb-nav-close}\\n in inline evaluation of: ``import java.util.List; import java.util.Random; import java.text.SimpleDateForma . . . \'\' at line number 24", 1, null, null, null, null], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": ["010 - Vuelve al men\u00FA principal", 1, 1, "500\/Sourced file: inline evaluation of: ``import java.util.List; import java.util.Random; import java.text.SimpleDateForma . . . \'\' : Method Invocation WDS.browser.findElement : at Line: 24 : in file: inline evaluation of: ``import java.util.List; import java.util.Random; import java.text.SimpleDateForma . . . \'\' : WDS .browser .findElement ( By .id ( &quot;sb-nav-close&quot; ) ) \\n\\nTarget exception: org.openqa.selenium.NoSuchElementException: no such element: Unable to locate element: {&quot;method&quot;:&quot;css selector&quot;,&quot;selector&quot;:&quot;#sb\\\\-nav\\\\-close&quot;}\\n  (Session info: headless chrome=97.0.4692.99)\\nFor documentation on this error, please visit: http:\\\/\\\/seleniumhq.org\\\/exceptions\\\/no_such_element.html\\nBuild info: version: \'3.14.0\', revision: \'aacccce0\', time: \'2018-08-02T20:19:58.91Z\'\\nSystem info: host: \'PRACTIACLNB0389\', ip: \'172.22.248.157\', os.name: \'Windows 10\', os.arch: \'x86\', os.version: \'10.0\', java.version: \'1.8.0_311\'\\nDriver info: org.openqa.selenium.chrome.ChromeDriver\\nCapabilities {acceptInsecureCerts: true, browserName: chrome, browserVersion: 97.0.4692.99, chrome: {chromedriverVersion: 97.0.4692.71 (adefa7837d02a..., userDataDir: C:\\\\Users\\\\CARLOS~1\\\\AppData\\\\L...}, goog:chromeOptions: {debuggerAddress: localhost:60920}, javascriptEnabled: true, networkConnectionEnabled: false, pageLoadStrategy: normal, platform: WINDOWS, platformName: WINDOWS, proxy: Proxy(direct), setWindowRect: true, strictFileInteractability: false, timeouts: {implicit: 0, pageLoad: 300000, script: 30000}, unhandledPromptBehavior: dismiss and notify, webauthn:extension:credBlob: true, webauthn:extension:largeBlob: true, webauthn:virtualAuthenticators: true}\\nSession ID: 9ca33f9fd56f1392d1b79247d966b032\\n*** Element info: {Using=id, value=sb-nav-close}\\n in inline evaluation of: ``import java.util.List; import java.util.Random; import java.text.SimpleDateForma . . . \'\' at line number 24", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": ["001 - Descarga Documento", 1, 1, "500\/SIN Documentos que descargar", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": ["004 - Ingreso Numero Serie", 1, 1, "500\/Sourced file: inline evaluation of: ``import java.util.List; import java.util.Random;  import org.openqa.selenium.WebE . . . \'\' : Typed variable declaration : Method Invocation WDS.browser.findElement : at Line: 19 : in file: inline evaluation of: ``import java.util.List; import java.util.Random;  import org.openqa.selenium.WebE . . . \'\' : WDS .browser .findElement ( By .xpath ( &quot;\\\/\\\/button[@class=\'ui button primary small continuaNumeroSerie primary-edt\']&quot; ) ) \\n\\nTarget exception: org.openqa.selenium.NoSuchElementException: no such element: Unable to locate element: {&quot;method&quot;:&quot;xpath&quot;,&quot;selector&quot;:&quot;\\\/\\\/button[@class=\'ui button primary small continuaNumeroSerie primary-edt\']&quot;}\\n  (Session info: headless chrome=97.0.4692.99)\\nFor documentation on this error, please visit: http:\\\/\\\/seleniumhq.org\\\/exceptions\\\/no_such_element.html\\nBuild info: version: \'3.14.0\', revision: \'aacccce0\', time: \'2018-08-02T20:19:58.91Z\'\\nSystem info: host: \'PRACTIACLNB0389\', ip: \'172.22.248.157\', os.name: \'Windows 10\', os.arch: \'x86\', os.version: \'10.0\', java.version: \'1.8.0_311\'\\nDriver info: org.openqa.selenium.chrome.ChromeDriver\\nCapabilities {acceptInsecureCerts: true, browserName: chrome, browserVersion: 97.0.4692.99, chrome: {chromedriverVersion: 97.0.4692.71 (adefa7837d02a..., userDataDir: C:\\\\Users\\\\CARLOS~1\\\\AppData\\\\L...}, goog:chromeOptions: {debuggerAddress: localhost:60920}, javascriptEnabled: true, networkConnectionEnabled: false, pageLoadStrategy: normal, platform: WINDOWS, platformName: WINDOWS, proxy: Proxy(direct), setWindowRect: true, strictFileInteractability: false, timeouts: {implicit: 0, pageLoad: 300000, script: 30000}, unhandledPromptBehavior: dismiss and notify, webauthn:extension:credBlob: true, webauthn:extension:largeBlob: true, webauthn:virtualAuthenticators: true}\\nSession ID: 9ca33f9fd56f1392d1b79247d966b032\\n*** Element info: {Using=xpath, value=\\\/\\\/button[@class=\'ui button primary small continuaNumeroSerie primary-edt\']}\\n in inline evaluation of: ``import java.util.List; import java.util.Random;  import org.openqa.selenium.WebE . . . \'\' at line number 19", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
